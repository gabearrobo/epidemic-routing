import ns.core
import ns.network
import ns.applications
import ns.mobility
import ns.wifi
import ns.internet
import ns.epidemic_routing
import sys

# General parameters
mobility_model = "RandomWaypoint"
nWifis = 10
txpDistance = 120.0
nodeSpeed = 50.0
app_logging = True
nodeContainer = ns.network.NodeContainer()
devices = ns.network.NetDeviceContainer()

# Epidemic parameters
epidemicHopCount = 50
epidemicQueueLength = 50
epidemicQueueEntryExpireTime = ns.core.Seconds (100)
epidemicBeaconInterval = ns.core.Seconds (1)

# Application parameters
rate = "0.512kbps"
packetSize = 64
appTotalTime = 100.0
appDataStart = 10.0
appDataEnd = 15
source_num = 0
sink_num = 9

if source_num >= nWifis:
  print("Source number can not exceed number of nodes")
  sys.exit(1)

if sink_num >= nWifis or source_num >= nWifis:
  print("Sink number can not exceed number of nodes")
  sys.exit(1)

print("Number of wifi nodes: " + str(nWifis))
print("Source number: " + str(source_num))
print("Sink number: " + str(sink_num))
print("Node speed: " + str(nodeSpeed) + " m/s")
print("Packet size: " + str(packetSize) + " b")
print("Transmission distance: " + str(txpDistance) + " m")
print("Hop count: " + str(epidemicHopCount))
print("Queue length: " + str(epidemicQueueLength) + " packets")
print("Queue entry expire time: " + str(epidemicQueueEntryExpireTime.GetSeconds ()) + " s")
print("Beacon interval: " + str(epidemicBeaconInterval.GetSeconds ()) + " s")

# Enabling OnOffApplication and PacketSink logging
if app_logging == True:
  ns.core.LogComponentEnable("OnOffApplication", ns.core.LOG_LEVEL_ALL)
  ns.core.LogComponentEnable("PacketSink", ns.core.LOG_LEVEL_ALL)
  ns.core.LogComponentEnable("EpidemicRoutingProtocol", ns.core.LOG_LEVEL_ALL)

nodeContainer.Create(nWifis)

# Mobility model Setup
mobility = ns.mobility.MobilityHelper()
if mobility_model == "Grid":
  internode_distance = 100
  mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
                                 "SetDeltaX", ns.core.DoubleValue(internode_distance))

elif mobility_model == "RandomWaypoint":
  mobility.SetPositionAllocator ("ns3::RandomRectanglePositionAllocator",
                                 "X", ns.core.StringValue ("ns3::UniformRandomVariable[Min=0.0|Max=300.0]"),
                                 "Y", ns.core.StringValue ("ns3::UniformRandomVariable[Min=0.0|Max=1500.0]"))

  mobility.SetMobilityModel ("ns3::SteadyStateRandomWaypointMobilityModel",
                             "MinSpeed", ns.core.DoubleValue (0.01),
                             "MaxSpeed", ns.core.DoubleValue (nodeSpeed),
                             "MinX", ns.core.DoubleValue (0.0),
                             "MaxX", ns.core.DoubleValue (300.0),
                             "MinPause", ns.core.DoubleValue (10),
                             "MaxPause", ns.core.DoubleValue (20),
                             "MinY", ns.core.DoubleValue (0.0),
                             "MaxY", ns.core.DoubleValue (1500.0))

mobility.Install (nodeContainer)

# Physical and link Layers Setup
wifiMac = ns.wifi.WifiMacHelper()
wifiMac.SetType ("ns3::AdhocWifiMac")
wifiPhy = ns.wifi.YansWifiPhyHelper()
wifiChannel = ns.wifi.YansWifiChannelHelper.Default()

wifiChannel.AddPropagationLoss ("ns3::RangePropagationLossModel",
                                "MaxRange", ns.core.DoubleValue (txpDistance))
wifiPhy.SetChannel (wifiChannel.Create ())
wifi = ns.wifi.WifiHelper()
wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                              "DataMode", ns.core.StringValue ("OfdmRate6Mbps"),
                              "RtsCtsThreshold", ns.core.UintegerValue (0))
devices = wifi.Install (wifiPhy, wifiMac, nodeContainer)

# Epidemic Routing Setup
epidemic = ns.epidemic_routing.EpidemicHelper()
epidemic.Set ("HopCount", ns.core.UintegerValue (epidemicHopCount))
epidemic.Set ("QueueLength", ns.core.UintegerValue (epidemicQueueLength))
epidemic.Set ("QueueEntryExpireTime", ns.core.TimeValue (epidemicQueueEntryExpireTime))
epidemic.Set ("BeaconInterval", ns.core.TimeValue (epidemicBeaconInterval))

# Internet Stack Setup
internet = ns.internet.InternetStackHelper()
internet.SetRoutingHelper (epidemic)
internet.Install (nodeContainer)
ipv4 = ns.internet.Ipv4AddressHelper()
ipv4.SetBase (ns.network.Ipv4Address("10.1.1.0"), ns.network.Ipv4Mask("255.255.255.0"))
interfaces = ipv4.Assign (devices)

# Application Setup
port = 9
appSource = ns.network.NodeList.GetNode(0)
appSink = ns.network.NodeList.GetNode(1)
remoteAddr = appSink.GetObject(ns.internet.Ipv4.GetTypeId()).GetAddress(1,0).GetLocal()

onoff1 = ns.applications.OnOffHelper("ns3::UdpSocketFactory",
                                     ns.network.Address(ns.network.InetSocketAddress(remoteAddr, port)))
onoff1.SetConstantRate (ns.network.DataRate (rate))
onoff1.SetAttribute ("PacketSize", ns.core.UintegerValue (packetSize))
apps1 = onoff1.Install (ns.network.NodeContainer(appSource))
apps1.Start (ns.core.Seconds (appDataStart))
apps1.Stop (ns.core.Seconds (appDataEnd))

sink = ns.applications.PacketSinkHelper("ns3::UdpSocketFactory",
                                        ns.network.InetSocketAddress (ns.network.Ipv4Address.GetAny (), port))
apps_sink = sink.Install (ns.network.NodeContainer(appSink))
apps_sink.Start (ns.core.Seconds (0.0))
apps_sink.Stop (ns.core.Seconds (appTotalTime))

ns.core.Simulator.Stop(ns.core.Seconds(10.0))

ns.core.Simulator.Run()
ns.core.Simulator.Destroy()